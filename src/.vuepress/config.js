module.exports = {
    base: '/dudu/',
    title: 'Memorial do  Dudu',
    description: `Cão amigo, companheiro e fiel.`,

    themeConfig: {
        logo: '/images/logo.jpg',
        logoNavbar: '/images/logo-navbar.jpg',
        nav: [
            { text: 'Inicio', link: '/' },
            { text: 'Sobre', link: '/sobre/' },
            { text: 'Fotos', link: '/fotos/' },
        ],
        sidebar: 'auto',


        repo: 'https://gitlab.com/da-ecom/dudu',
        repoLabel: 'GitLab',
    }
}

