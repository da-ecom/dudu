---
#title: Fotos do dudu
#date: 2018-08-01 20:23:04
#excerpt: Fotos. 
---


# Fotos


<div class="img-grid">
    <ul>
        <li> <img src="./dudu-01.jpg" /> </li>
        <li> <img src="./dudu-02.jpg" /> </li>
        <li> <img src="./dudu-03.jpg" /> </li>
        <li> <img src="./dudu-04.jpg" /> </li>
        <li> <img src="./dudu-05.jpg" /> </li>
        <li> <img src="./dudu-06.jpg" /> </li>
        <li> <img src="./dudu-07.jpg" /> </li>
        <li> <img src="./dudu-08.jpg" /> </li>
        <li> <img src="./dudu-09.jpg" /> </li>
        <li> <img src="./dudu-10.jpg" /> </li>
        <li> <img src="./dudu-01.jpg" /> </li>
        <li> <img src="./dudu-02.jpg" /> </li>
    </ul>

</div>



<style>

.img-grid img {
    max-width: 100%;
    border-radius: 5px;
    border: #e0e0e0 solid 1px;
}

.img-grid ul {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-gap: 30px;

    list-style: none;
}
.img-grid ul li {
    display: flex;
    flex-direction: column;
}


@media only screen and (max-width: 959px) {
    .img-grid ul {
        grid-template-columns: 1fr 1fr;
    }
}

@media only screen and (max-width: 719px) {
    .img-grid ul {
        grid-template-columns: 1fr;
    }
}

</style>