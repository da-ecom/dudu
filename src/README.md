---
home: true
heroImage: /images/logo.jpg
actionText: Sobre
actionLink: /sobre/
features:
- title: Início da história
  details: Dudu morou no novo Campus do CEFET Timóteo desde o início de sua construção.
- title: Sempre presente
  details: Esteve presente por 10 anos, sendo uma companhia fiel para os alunos.
- title: Falecimento
  details: Faleceu no dia 20 de Maio de 2019, momento em que estava infectado por Leishmaniose.
footer: DA-ECOM | Copyright © 2019
---